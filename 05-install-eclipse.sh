#! /bin/bash

#author: Pierre-Yves Lemay
HERE=$(pwd)
. dev_user.sh

#https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2019-06/R/eclipse-jee-2019-06-R-linux-gtk-x86_64.tar.gz&mirror_id=1264

URL="http://mirror.csclub.uwaterloo.ca/eclipse/technology/epp/downloads/release/2019-09/R/eclipse-jee-2019-09-R-linux-gtk-x86_64.tar.gz"
#ECLIPSE_ARCHIVE="eclipse-jee-2019-09-R-linux-gtk-x86_64.tar.gz"
ECLIPSE_NAME="jee-2019-09-R"

cd /opt/dev/java

#if directory /opt/dev/java/eclipses does not exists, create it.
#same for /opt/dev/java/eclipses/oxygen3
if [ ! -d /opt/dev/java/eclipses ]
then
  echo "creating eclipses directory"
  mkdir eclipses
fi
cd eclipses
mkdir "$ECLIPSE_NAME"
cd "$ECLIPSE_NAME"

echo "....getting eclipse from internet..."
wget -4 "$URL" -qO- | tar -xzf -
#cp $ECLIPSE_ARCHIVE /opt/dev/java/eclipses/"$ECLIPSE_NAME"/"$ECLIPSE_ARCHIVE"
#rm "$ECLIPSE_ARCHIVE"

#install
#echo "....installing eclipse '$ECLIPSE_NAME'"
#cd /opt/dev/java/eclipses/"$ECLIPSE_NAME"
#tar -xzf "$ECLIPSE_ARCHIVE"
#rm "$ECLIPSE_ARCHIVE"

#symlink to default eclipse version (directory)
ln -s /opt/dev/java/eclipses/"$ECLIPSE_NAME"/eclipse /opt/dev/java/eclipses/eclipse
cp $HERE/code-formatter.xml /opt/dev/java/eclipses/"$ECLIPSE_NAME"/code-formatter.xml

cd $HERE
