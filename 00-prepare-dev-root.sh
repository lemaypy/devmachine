#!/bin/bash

#author: Pierre-Yves Lemay

#HERE= where this script is located in the file system
HERE=$(pwd)

. dev_user.sh


cd /opt

#creating de /opt/dev directory. If it already exists, delete it and start the whole install procedure
if [ ! -d /opt/dev ]
then
  echo "dev non existant"
  sudo mkdir dev
  sudo chown -R $DEV_USER:$DEV_USER dev
else
  echo "dev existe"
fi

if [ -d /opt/dev/java ]
then
  echo "java existe. On détruit"
  sudo rm -rf /opt/dev/java
fi
echo "on crée java"
sudo mkdir -p /opt/dev/java

sudo mkdir -p /opt/dev/java/jdks
sudo mkdir -p /opt/dev/java/maven
sudo mkdir -p /opt/dev/java/servers

sudo chown -R $DEV_USER:$DEV_USER /opt/dev/java
cd $HERE
# copy all install scripts from current directory (clone from gitlab/github) to /opt/install-dev
#if [ -d /opt/install-dev ]
#then
#  sudo rm -rf /opt/install-dev
#fi
#sudo mkdir install-dev
#sudo chown -R $DEV_USER:$DEV_USER /opt/install-dev

#sudo cp $HERE/*.sh /opt/install-dev
#sudo cp $HERE/*.xml /opt/install-dev
#sudo cp $HERE/*.tar.gz /opt/install-dev
#sudo chmod +x /opt/install-dev/*.sh
