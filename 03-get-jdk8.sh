#!/bin/bash

#author: Pierre-Yves Lemay

jdkname="jdk-8u212-linux-x64.tar.gz"
URL="https://download.oracle.com/otn/java/jdk/8u212-b10/59066701cf1a433da9770636fbc4c9aa/jdk-8u212-linux-x64.tar.gz"
if [ -e /opt/depot/${jdkname} ]
then
echo "....${jdkname} already in depot, copying to jdks..."
  cp /opt/depot/${jdkname} /opt/dev/java/jdks/${jdkname}
else
  echo "....download ${jdkname} from internet..."
  cd /opt/depot
  wget -4 --no-cookies \
  --no-check-certificate -c \
  --header "Cookie: oraclelicense=accept-securebackup-cookie" \
  "$URL" \
  -O ${jdkname}
  cp /opt/depot/${jdkname} /opt/dev/java/jdks/${jdkname}
fi

echo "....installing ${jdkname}..."
cd /opt/dev/java/jdks
tar -xzf ${jdkname}
rm ${jdkname}
