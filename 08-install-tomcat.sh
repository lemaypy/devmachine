#!/bin/bash

#author: Pierre-Yves Lemay
HERE=$(pwd)

if [ ! -d /opt/dev/java/servers ]
then
  mkdir /opt/dev/java/servers
fi

#tomcat 8

TOMCAT8_URL="http://apache.mirror.iweb.ca/tomcat/tomcat-8/v8.5.47/bin/apache-tomcat-8.5.47.tar.gz"
#TOMCAT8_ARCHIVE="apache-tomcat-8.5.47.tar.gz"
TOMCAT9_URL="http://httpd-mirror.sergal.org/apache/tomcat/tomcat-9/v9.0.27/bin/apache-tomcat-9.0.27.tar.gz"
#TOMCAT9_ARCHIVE="apache-tomcat-9.0.27.tar.gz"


#if [ -e /opt/depot/"$TOMCAT8_ARCHIVE" ]
#then
#  echo "...."$TOMCAT8_ARCHIVE" already in depot, copying to servers..."
#  cp /opt/depot/"$TOMCAT8_ARCHIVE" /opt/dev/java/servers/"$TOMCAT8_ARCHIVE"
#else
cd /opt/dev/java/servers
echo "....download TOMCAT 8 ..."
wget -4 "$TOMCAT8_URL" -O- | tar -xzf -
echo "....download TOMCAT 9 ..."
wget -4 "$TOMCAT9_URL" -O- | tar -xzf -

cd $HERE
#  -O "$TOMCAT8_ARCHIVE"
#  cp "$TOMCAT8_ARCHIVE" /opt/dev/java/servers/"$TOMCAT8_ARCHIVE"
#  rm "$TOMCAT8_ARCHIVE"
#fi

#tomcat 9


#if [ -e /opt/depot/"$TOMCAT9_ARCHIVE" ]
#then
#  echo "...."$TOMCAT9_ARCHIVE" already in depot, copying to servers..."
#  cp /opt/depot/"$TOMCAT9_ARCHIVE" /opt/dev/java/servers/"$TOMCAT9_ARCHIVE"
#else
#  echo "....download "$TOMCAT9_ARCHIVE" from internet..."
#  cd /opt/depot
#  wget -4 "$TOMCAT9_URL" \
#  -O "$TOMCAT9_ARCHIVE"
#  cp "$TOMCAT9_ARCHIVE" /opt/dev/java/servers/"$TOMCAT9_ARCHIVE"
#  rm "$TOMCAT9_ARCHIVE"
#fi

#echo "....installing $TOMCAT8_ARCHIVE"
#cd /opt/dev/java/servers
#tar -xzf "$TOMCAT8_ARCHIVE"
#echo "....installing $TOMCAT9_ARCHIVE..."
#tar -xzf "$TOMCAT9_ARCHIVE"
#menage
#rm "$TOMCAT8_ARCHIVE"
#rm "$TOMCAT9_ARCHIVE"
#
