#! /bin/bash

#author: Pierre-Yves Lemay

HERE=$(pwd)

if [ "$USER" == "root" ]; then
  echo "Please restart with developer user account" && exit 1
fi

echo "install......................................libs"

cp libs.tar.gz /opt/dev/java/libs.tar.gz
cd /opt/dev/java
tar -xzf libs.tar.gz
rm libs.tar.gz
cd $HERE
