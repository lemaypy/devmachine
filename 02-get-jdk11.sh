#!/bin/bash

#author: Pierre-Yves Lemay
HERE=$(pwd)
. dev_user.sh

jdkname="jdk-13.0.1"
jdk_tar_name="openjdk-13.0.1_linux-x64_bin.tar.gz"
openjdk_url="https://download.java.net/java/GA/jdk13.0.1/cec27d702aa74d5a8630c65ae61e4305/9/GPL/openjdk-13.0.1_linux-x64_bin.tar.gz"

if [ -e /opt/dev/java/jdks/${jdkname} ]
then
  echo "....${jdkname} already in development environment, nothing to do..."
  exit 0
fi

echo "....download ${jdkname} ....."
cd /opt/dev/java/jdks
wget -4 --no-cookies \
    --no-check-certificate \
    --header "Cookie: oraclelicense=accept-securebackup-cookie" \
    ${openjdk_url} -qO- | tar -xzf -

echo "....create symlink /opt/dev/java/jdks/jdk. Target is /opt/dev/java/jdks/"${jdkname}
ln -s /opt/dev/java/jdks/${jdkname} /opt/dev/java/jdks/jdk

cd $HERE
