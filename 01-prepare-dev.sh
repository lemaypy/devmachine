#!/bin/bash

#author: Pierre-Yves Lemay
HERE=$(pwd)
. dev_user.sh

if [ "$USER" == "root" ]; then
  echo "Please restart with developer user account" && exit 1
fi

do_0="false"
do_2="false"
do_3="false"
do_4="false"
do_5="false"
do_6="false"
do_7="true"
do_8="false"
do_12="false"

if [[ "${do_0}" = "true" ]]; then
  echo "Calling 00-prepare-dev-root.sh"
  ./00-prepare-dev-root.sh
fi
if [[ "${do_2}" = "true" ]]; then
  echo "Calling 02-get-jdk11.sh"
  ./02-get-jdk11.sh
fi

if [[ "${do_3}" = "true" ]]; then
  echo "Calling 03-get-jdk8.sh"
  ./03-get-jdk8.sh
fi

if [[ "${do_4}" = "true" ]]; then
  echo "Calling 04-install-maven.sh"
  ./04-install-maven.sh
fi

if [[ "${do_5}" = "true" ]]; then
  echo "Calling 05-install-eclipse.sh"
  ./05-install-eclipse.sh
fi

if [[ "${do_6}" = "true" ]]; then
  echo "Calling 06-create-workspaces.sh"
  ./06-create-workspaces.sh
fi

if [[ "${do_7}" = "true" ]]; then
  echo "Calling 07-create-scripts.sh"
  ./07-create-scripts.sh
fi

if [[ "${do_8}" = "true" ]]; then
  echo "Calling 08-install-tomcat.sh"
  ./08-install-tomcat.sh
fi
if [[ "${do_12}" = "true" ]]; then
  echo "Calling 12-install-libs.sh"
  ./12-install-libs.sh
fi


#echo "........calling 035-set-default-jdk.sh"
#/opt/install-dev/035-set-default-jdk.sh

#echo "........calling 06-create-workspaces.sh"
#/opt/install-dev/06-create-workspaces.sh
#echo "........calling 07-create-scripts.sh"
##/opt/install-dev/07-create-scripts.sh
#echo "........calling 08-install-tomcat.sh"
#/opt/install-dev/08-install-tomcat.sh
#echo "........calling 09-install-tomee.sh"
#/opt/install-dev/09-install-tomee.sh

#if [ -d /etc/xfce4 ]
#then
#  echo "........setup xfce thunar for executing .sh files on double-click"
#  /opt/install-dev/10-xfce-settings.sh
#fi

#echo "........calling 11-install-javafx.sh"
#/opt/install-dev/11-install-javafx.sh
#echo "........calling 12-install-libs.sh"
#/opt/install-dev/12-install-libs.sh
#echo "........calling 13-install-docker.sh"
#/opt/install-dev/13-install-docker.sh

echo "....01-prepare-dev.sh Done!"
cd $HERE
# echo "....Please restart to terminate the installation"
